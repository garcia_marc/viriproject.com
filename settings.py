import os
from lib.middleware.appendslash import AppendSlashMiddleware


devel_str = 'Google App Engine Development'
server_software = os.environ.get('SERVER_SOFTWARE')
DEBUG = not (server_software and server_software[:len(devel_str)] != devel_str)

I18N = False
JINJA2_TEMPLATES = [os.path.join(os.path.dirname(__file__), 'templates')]
JINJA2_EXTENSIONS = []
MIDDLEWARE = (AppendSlashMiddleware,)

