from google.appengine.ext import webapp
from lib.template import render_response
from models import Post

class Base(webapp.RequestHandler):
    def get(self):
        render_response(self, self.template)


class Home(Base):
    template = 'home.html'

class Installation(Base):
    template = 'installation.html'

class Community(Base):
    template = 'community.html'

class Blog(webapp.RequestHandler):
    def get(self):
        posts = Post.all().order('-posted').fetch(5)

        # if no posts, create a dummy one so more can be added from the datastore
        if not len(posts):
            import datetime
            Post(title='TITLE', content='CONTENT', posted=datetime.datetime.now()).put()

        render_response(self, 'blog.html', dict(posts=posts))

class ScriptRepo(Base):
    template = 'script_repo.html'

