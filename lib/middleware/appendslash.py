class AppendSlashMiddleware(object):
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        def _start_response(status, headers, exc_info=None):
            if environ['PATH_INFO'][-1] != '/':
                qs = environ['QUERY_STRING']
                qs = qs and '?' + qs
                return start_response('302 Found', (('Location', environ['PATH_INFO'] + '/' + qs),), exc_info)
            else:
                return start_response(status, headers, exc_info)

        self.app(environ, _start_response)

