from google.appengine.ext import webapp
from template import render_response

def http404(self):
    self.error(404)
    render_response(self, '404.html')

class Http404(Exception):
    pass

class NotFoundHandler(webapp.RequestHandler):
    def get(self):
        http404(self)

    def post(self):
        http404(self)

