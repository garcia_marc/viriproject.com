import sys
import os
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app

def main():
    sys.path.append(os.path.join(os.path.dirname(__file__), 'ext'))
    from lib.http import NotFoundHandler
    import urls
    import settings

    urlmap = urls.urlmap + (('^.*$', NotFoundHandler),)
    application = webapp.WSGIApplication(urlmap, debug=settings.DEBUG)
    for middleware in settings.MIDDLEWARE:
        application = middleware(application)
    run_wsgi_app(application)

if __name__ == '__main__':
    main()

