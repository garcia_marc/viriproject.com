import handlers

urlmap = (
    (r'^/$', handlers.Home),
    (r'^/installation/$', handlers.Installation),
    (r'^/community/$', handlers.Community),
    (r'^/blog/$', handlers.Blog),
    (r'^/script-repo/$', handlers.ScriptRepo),
)

