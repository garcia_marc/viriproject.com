from google.appengine.ext import db

class Post(db.Model):
    title = db.StringProperty()
    content = db.TextProperty()
    posted = db.DateTimeProperty(indexed=True)

    @property
    def posted_with_format(self):
        return self.posted.strftime('%A, %B %d, %Y')

